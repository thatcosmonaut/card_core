module CardCore
  class Cribbage < Game
    SCORE_VALUES = { ace: 1,
                     two: 2,
                     three: 3,
                     four: 4,
                     five: 5,
                     six: 6,
                     seven: 7,
                     eight: 8,
                     nine: 9,
                     ten: 10,
                     jack: 10,
                     queen: 10,
                     king: 10
                   }

    attr_accessor :crib
    attr_accessor :player_one_score
    attr_accessor :player_two_score

    def initialize(players, deck, verbose = false)
      super
      @verbose = verbose

      @phases = [:discard, :peg, :show]
      @crib = []

      @deck = deck
      @deck.shuffle

      @player_one = players[0]
      @player_two = players[1]
      @dealer = [@player_one, @player_two].sample
      @not_dealer = @dealer == @player_one ? @player_two : @player_one

      @player_one_score = 0
      @player_two_score = 0

      @peg_card_list = []
      @peg_count = 0
    end

    def deal
      puts "the cards are dealt" if @verbose
      6.times do
        card = @deck.draw
        @not_dealer.add_card(card)
        card = @deck.draw
        @dealer.add_card(card)
      end
    end

    def run
      puts "#{@dealer.name} selected as the dealer" if @verbose

      loop do
        @peg_count = 0
        @peg_card_list = []

        puts "the deck is shuffled" if @verbose
        @deck.shuffle

        deal

        # discard phase
        not_dealer_discard_choices = @not_dealer.ai.discard_choices(@not_dealer.cards)
        raise 'invalid choice' unless not_dealer_discard_choices.all? { |card| @not_dealer.cards.include?(card) }

        dealer_discard_choices = @dealer.ai.discard_choices(@dealer.cards)
        raise 'invalid choice' unless dealer_discard_choices.all? { |card| @dealer.cards.include?(card) }

        puts "#{@not_dealer.name} places two cards in the crib"
        not_dealer_discard_choices.each { |card| make_discard_play(@not_dealer, card) }

        puts "#{@dealer.name} places two cards in the crib"
        dealer_discard_choices.each { |card| make_discard_play(@dealer, card) }

        #should i implement cutting the deck here to make it truer to the rules? idk if it matters
        starter_card = @deck.draw
        puts "starter card is revealed to be the #{starter_card}" if @verbose
        if starter_card.value == :jack
          add_score(@dealer, 2)
          puts "#{@dealer.name} receives 2 points for nibs" if @verbose
          break if winner?
        end

        current_turn_player = @not_dealer
        passed = false
        last_card_player = nil
        #passed_player = nil

        # peg phase
        puts "peg phase begins" if @verbose

        until @dealer.empty_hand? and @not_dealer.empty_hand?
          current_turn_legal_moves = legal_peg_moves(current_turn_player, @peg_card_list)
          if current_turn_legal_moves.empty?
            if passed
              add_score(last_card_player, 1)
              if @verbose
                puts "#{last_card_player.name} scores 1 for the go"
              end
              break if winner?

              @peg_card_list = []
              passed = false
              current_turn_player = other_player(current_turn_player)
            else
              passed = true
              puts "#{current_turn_player.name} passes"
              current_turn_player = other_player(current_turn_player)
            end
          else
            move_choice = current_turn_player.ai.peg_choice(current_turn_legal_moves, @peg_card_list)
            raise "invalid choice" unless current_turn_legal_moves.include?(move_choice)

            puts "#{current_turn_player.name} plays #{move_choice}"
            score = make_peg_play(current_turn_player, move_choice)
            add_score(current_turn_player, score)
            break if winner?

            last_card_player = current_turn_player

            if self.class.peg_count(@peg_card_list) == 31
              @peg_card_list = []
              passed = false
            end

            current_turn_player = other_player(current_turn_player) unless passed
          end
        end
        break if winner? #to break outer loop oh man this is ugly as hell

        unless self.class.peg_count(@peg_card_list) == 0 #ending on 31-count
          add_score(last_card_player, 1)
          puts "#{last_card_player.name} scores 1 for the go" if @verbose
          break if winner?
        end

        #show phase
        puts "show phase begins" if @verbose
        @not_dealer.return_played_cards_to_hand
        @dealer.return_played_cards_to_hand

        if @verbose
          puts "#{@not_dealer.name} counts hand"
          puts @not_dealer.cards.join(', ')
        end

        add_score(@not_dealer, self.class.score_show(@not_dealer.cards, starter_card, @verbose))
        break if winner?

        if @verbose
          puts "#{@dealer.name} counts hand"
          puts @dealer.cards.join(', ')
        end

        add_score(@dealer, self.class.score_show(@dealer.cards, starter_card, @verbose))
        break if winner?

        if @verbose
          puts "#{@not_dealer.name} reveals the crib"
          puts @crib.join(', ')
        end

        add_score(@not_dealer, self.class.score_show(@crib, starter_card, @verbose))
        break if winner?

        if @verbose
          puts "#{@player_one.name}: #{@player_one_score}"
          puts "#{@player_two.name}: #{@player_two_score}"
        end

        @deck.add_cards_to_bottom(@dealer.cards)
        @deck.add_cards_to_bottom(@not_dealer.cards)
        @deck.add_cards_to_bottom(@crib)

        @dealer.clear_hand
        @not_dealer.clear_hand
        @crib.clear

        @dealer, @not_dealer = @not_dealer, @dealer
        puts "#{@dealer.name} is now the dealer" if @verbose
      end

      puts "#{@player_one.name}: #{@player_one_score}"
      puts "#{@player_two.name}: #{@player_two_score}"
    end

    def winner?
      @player_one_score >= 121 or @player_two_score >= 121
    end

    def other_player(player)
      if player == @not_dealer
        @dealer
      else
        @not_dealer
      end
    end

    def legal_peg_moves(player, peg_card_list)
      current_peg_count = self.class.peg_count(peg_card_list)
      player.cards.reject { |card| SCORE_VALUES[card.value] + current_peg_count > 31 }
    end

    def add_score(player, amount)
      if player == @player_one
        @player_one_score += amount
      else
        @player_two_score += amount
      end
    end

    def make_discard_play(player, card)
      player.discard(card)
      @crib << card
    end

    def make_peg_play(player, card)
      player.play_card(card)
      @peg_card_list << card
      self.class.peg_score(@peg_card_list, @verbose)
    end

    class << self
      def card_score_value(card)
        SCORE_VALUES[card.value]
      end

      def peg_count(card_list)
        card_list.empty? ? 0 : card_list.map { |card| SCORE_VALUES[card.value] }.inject(:+)
      end

      def peg_score(card_list, verbose = false)
        count_score = peg_count_score(card_list, verbose)
        matching_kind_score = peg_matching_kind_score(card_list, verbose)
        run_score = peg_run_score(card_list, verbose)
        count_score + matching_kind_score + run_score
      end

      def peg_count_score(card_list, verbose = false)
        current_peg_count = peg_count(card_list)
        if current_peg_count == 15
          puts "scores 2 for 15" if verbose
          return 2
        elsif current_peg_count == 31
          puts "scores 2 for 31" if verbose
          return 2
        else
          return 0
        end
      end

      def peg_matching_kind_score(card_list, verbose = false)
        values = card_values(card_list)
        if card_list.count >= 4 and values.last(4).uniq.count == 1
          puts "scores 12 for match" if verbose
          return 12
        elsif card_list.count >= 3 and values.last(3).uniq.count == 1
          puts "scores 6 for match" if verbose
          return 6
        elsif card_list.count >= 2 and values.last(2).uniq.count == 1
          puts "scores 2 for match" if verbose
          return 2
        else
          return 0
        end
      end

      def peg_run_score(card_list, verbose = false)
        (3..card_list.length).reverse_each do |len|
          card_list.last(len).permutation.each do |set|
            if set.each_cons(2).map { |x, y| x.succ_value == y.value}.all?
              puts "scores #{len} for run" if verbose
              return len
            end
          end
        end

        return 0
      end

      def score_show(card_list, starter_card, verbose = false)
        count_score = show_count_score(card_list + [starter_card], verbose)
        match_score = show_match_score(card_list + [starter_card], verbose)
        run_score = show_run_score(card_list + [starter_card], verbose)
        flush_score = show_flush_score(card_list, starter_card, verbose)
        nob_score = show_nob_score(card_list, starter_card, verbose)
        count_score + match_score + run_score + flush_score + nob_score
      end

      def score_show_without_starter(card_list, verbose = false)
        count_score = show_count_score(card_list)
        match_score = show_match_score(card_list)
        run_score = show_run_score(card_list)
        flush_score = show_flush_score_without_starter(card_list)
        count_score + match_score + run_score + flush_score
      end

      def show_count_score(card_list, verbose = false)
        values = card_values(card_list).map { |value| SCORE_VALUES[value] }
        (2..4).map do |set_length|
          values.combination(set_length).map do |set|
            total = set.inject(:+)
            if verbose
              if total == 15
                puts "#{set.join (' and ')} make 15 for 2"
              elsif
                total == 31
                puts "#{set.join (' and ')} make 31 for 2"
              end
            end
            total == 15 or total == 31
          end
        end.flatten.count(true) * 2
      end

      def show_match_score(card_list, verbose = false)
        values = card_values(card_list)
        values.group_by { |x| x }.map do |value, set|
          if set.count == 2
            puts "pair of #{Card.pluralize_value(value)} for 2" if verbose
            2
          elsif set.count == 3
            puts "three #{Card.pluralize_value(value)} for 6" if verbose
            6
          elsif set.count == 4
            puts "four #{Card.pluralize_value(value)} for 12" if verbose
            12
          else
            0
          end
        end.inject(:+)
      end

      def show_run_score(card_list, verbose = false)
        runs = (3..card_list.length).map do |length|
          card_list.permutation(length).map do |set|
            set.each_cons(2).map do |x, y|
              x.succ_value == y.value
            end.all?
          end.count(true)
        end
        index = runs.rindex { |x| x > 0 }
        score = index.nil? ? 0 : (index + 3) * runs[index]
        if verbose and score > 0
          puts "#{runs[index]} run#{runs[index] > 1 ? 's' : nil} of #{index + 3} for #{score}"
        end
        score
      end

      def show_flush_score(card_list, starter_card, verbose = false)
        suits = card_list.map(&:suit)
        if suits.uniq.count == 1
          if starter_card.suit == card_list.first.suit
            puts "5-card flush for 5" if verbose
            5
          else
            puts "4-card flush for 4" if verbose
            4
          end
        else
          0
        end
      end

      def show_flush_score_without_starter(card_list)
        suits = card_list.map(&:suit)
        if suits.uniq.count == 1
          4
        else
          0
        end
      end

      def show_nob_score(card_list, starter_card, verbose = false)
        result = card_list.any? { |card| card.value == :jack and card.suit == starter_card.suit }
        puts "1 for his nob" if result and verbose
        result ? 1 : 0
      end

      def card_values(card_list)
        card_list.map(&:value)
      end
    end
  end
end
