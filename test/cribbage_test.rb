require './card_core'

module CardCore
  class CribbageTest
    def self.run
      player_one = CardCore::Player.new(CardCore::DummyCribbageCore, "Dummy One")
      player_two = CardCore::Player.new(CardCore::CryptageCribbageCore, "Evan AI")
      deck = CardCore::Deck.new

      cribbage_game = CardCore::Cribbage.new([player_one, player_two], deck, true)

      cribbage_game.run
      # cribbage_game.crib.each { |card| puts card }
      # player_one.played_cards.each { |card| puts card }
      # player_two.played_cards.each { |card| puts card }
      # puts cribbage_game.player_one_score
      # puts cribbage_game.player_two_score
    end
  end
end

CardCore::CribbageTest.run
