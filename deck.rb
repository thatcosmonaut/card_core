module CardCore
  class Deck
    def self.standard_cards
      [].tap do |cards|
        CardCore::SUITS.product(CardCore::VALUES).each do |suit, value|
          cards << Card.new(suit, value)
        end
      end
    end

    def initialize(cards = nil)
      @cards = cards ||= Deck.standard_cards
    end

    def shuffle
      @cards.shuffle!
    end

    def draw
      @cards.delete_at(0)
    end

    def add_cards_to_bottom(cards)
      cards.each { |card| @cards << card }
    end

    def add_to_bottom(card)
      @cards << card
    end

    def remaining_cards_count
      @cards.count
    end
  end
end
