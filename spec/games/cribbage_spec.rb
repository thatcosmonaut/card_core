require 'spec_helper'

describe CardCore::Cribbage do
  let(:ai_klass) { class_double('AiCore') }
  let(:player_one) { CardCore::Player.new(ai_klass, "Player One") }
  let(:player_two) { CardCore::Player.new(ai_klass, "Player Two") }
  let(:deck) { CardCore::Deck.new }
  let(:cribbage) { CardCore::Cribbage.new([player_one, player_two], deck) }
  let(:card_one) { CardCore::Card.new(:heart, :jack) }
  let(:card_two) { CardCore::Card.new(:heart, :queen) }

  describe 'deal' do
    before(:each) do
      cribbage.deal
    end

    it 'should deal six cards to player one' do
      expect(player_one.cards.count).to eql(6)
    end

    it 'should deal six cards to player two' do
      expect(player_one.cards.count).to eql(6)
    end

    it 'should remove 12 cards from the deck' do
      expect(deck.remaining_cards_count).to eql(40)
    end
  end

  describe 'card_score_value' do
    it 'returns the score value of the card' do
      card = CardCore::Card.new(:heart, :jack)
      expect(CardCore::Cribbage.card_score_value(card)).to eql(10)
    end
  end

  describe 'other_player' do
    it 'returns the other player' do
      expect(cribbage.other_player(player_one)).to eql(player_two)
    end
  end

  describe 'legal_peg_moves' do
    it 'returns legal peg moves for player' do
      card_one = CardCore::Card.new(:heart, :jack)
      card_two = CardCore::Card.new(:heart, :queen)
      card_three = CardCore::Card.new(:heart, :king)
      card_four = CardCore::Card.new(:heart, :ace)
      card_five = CardCore::Card.new(:heart, :five)

      peg_card_list = [card_one, card_two, card_three]

      player_one.add_card(card_four)
      player_one.add_card(card_five)
      expect(cribbage.legal_peg_moves(player_one, peg_card_list)).to eql([card_four])
    end
  end

  describe 'add_score' do
    it 'adds score to player one' do
      cribbage.add_score(player_one, 2)
      expect(cribbage.player_one_score).to eql 2
    end

    it 'adds score to player two' do
      cribbage.add_score(player_two, 2)
      expect(cribbage.player_two_score).to eql 2
    end
  end

  describe 'make_discard_play' do
    before(:each) do
      cribbage.deal
    end

    it 'should take a card from the player' do
      card = player_one.cards.first
      cribbage.make_discard_play(player_one, card)
      expect(player_one.cards.count).to eql(5)
    end

    it 'should add the card to the crib' do
      card = player_one.cards.first
      cribbage.make_discard_play(player_one, card)

      expect(cribbage.crib.first).to eql card
    end
  end

  describe 'make_peg_play' do
    before(:each) do
      player_one.add_card(card_one)
      cribbage.make_peg_play(player_one, card_one)
    end

    it 'removes card from player' do
      expect(player_one.cards).to_not include(card_one)
    end

    it 'adds card to player played card list' do
      expect(player_one.played_cards).to include(card_one)
    end

    it 'adds card to peg card list' do
      expect(cribbage.instance_variable_get(:@peg_card_list)).to include(card_one)
    end

    #this test sucks
    it 'scores' do
      expect(cribbage.player_one_score).to eql 0
    end
  end

  describe 'peg_count_score' do
    context 'peg score is 15' do
      it 'returns 2' do
        card_two = CardCore::Card.new(:spade, :five)

        expect(CardCore::Cribbage.peg_count_score([card_one, card_two])).to eql 2
      end
    end

    context 'peg score is 31' do
      it 'returns 2' do
        card_two = CardCore::Card.new(:heart, :queen)
        card_three = CardCore::Card.new(:heart, :king)
        card_four = CardCore::Card.new(:heart, :ace)

        cribbage.make_peg_play(player_one, card_one)
        cribbage.make_peg_play(player_two, card_two)
        cribbage.make_peg_play(player_one, card_three)
        cribbage.make_peg_play(player_two, card_four)

        expect(CardCore::Cribbage.peg_count_score([card_one, card_two, card_three, card_four])).to eql 2
      end
    end

    context 'peg score is not 15 or 31' do
      it 'returns 0' do
        card_two = CardCore::Card.new(:heart, :ace)

        expect(CardCore::Cribbage.peg_count_score([card_one, card_two])).to eql 0
      end
    end
  end

  describe 'peg_matching_kind_score' do
    let(:card_two) { CardCore::Card.new(:spade, :jack) }
    let(:card_three) { CardCore::Card.new(:diamond, :jack) }
    let(:card_four) { CardCore::Card.new(:club, :jack) }

    context 'no match' do
      it 'returns 0' do
        card_two = CardCore::Card.new(:spade, :queen)
        expect(CardCore::Cribbage.peg_matching_kind_score([card_one, card_two])).to eql 0
      end
    end

    context 'pair' do
      it 'returns 2' do
        expect(CardCore::Cribbage.peg_matching_kind_score([card_one, card_two])).to eql 2
      end
    end

    context 'set of three' do
      it 'returns 6' do
        expect(CardCore::Cribbage.peg_matching_kind_score([card_one, card_two, card_three])).to eql 6
      end
    end

    context 'set of four' do
      it 'returns 12' do
        expect(CardCore::Cribbage.peg_matching_kind_score([card_one, card_two, card_three, card_four])).to eql 12
      end
    end
  end

  describe 'peg_run_score' do
    let(:card_two) { CardCore::Card.new(:club, :ten) }

    context 'no run' do
      it 'returns 0' do
        card_three = CardCore::Card.new(:club, :ace)
        expect(CardCore::Cribbage.peg_run_score([card_one, card_two, card_three])).to eql 0
      end
    end

    context 'three-card run' do
      let(:card_three) { CardCore::Card.new(:club, :nine) }

      context 'in any order' do
        let(:orderings) { [card_one, card_two, card_three].permutation }

        it 'returns 3' do
          expect(orderings.map { |list| CardCore::Cribbage.peg_run_score(list) }).to all(eql 3)
        end
      end
    end

    context 'four-card run' do
      let(:card_three) { CardCore::Card.new(:club, :nine) }
      let(:card_four) { CardCore::Card.new(:diamond, :eight) }

      context 'in any order' do
        let(:orderings) { [card_one, card_two, card_three, card_four].permutation }

        it 'returns 4' do
          expect(orderings.map { |list| CardCore::Cribbage.peg_run_score(list) }).to all(eql 4)
        end
      end
    end

    context 'five-card run' do
      let(:card_three) { CardCore::Card.new(:club, :nine) }
      let(:card_four) { CardCore::Card.new(:diamond, :eight) }
      let(:card_five) { CardCore::Card.new(:spade, :seven) }

      context 'in any order' do
        let(:orderings) { [card_one, card_two, card_three, card_four, card_five].permutation }

        it 'returns 5' do
          expect(orderings.map { |list| CardCore::Cribbage.peg_run_score(list) }).to all(eql 5)
        end
      end
    end

    context 'six-card run' do
      let(:card_one) { CardCore::Card.new(:spade, :ace) }
      let(:card_two) { CardCore::Card.new(:heart, :two) }
      let(:card_three) { CardCore::Card.new(:diamond, :three) }
      let(:card_four) { CardCore::Card.new(:club, :four) }
      let(:card_five) {CardCore::Card.new(:heart, :five) }
      let(:card_six) { CardCore::Card.new(:diamond, :six) }

      context 'in any order' do
        let(:orderings) { [card_one, card_two, card_three, card_four, card_five, card_six].permutation }

        it 'returns 6' do
          expect(orderings.map { |list| CardCore::Cribbage.peg_run_score(list) }).to all(eql 6)
        end
      end
    end

    # current algorithm is too slow, probably need to implement card sort
    xcontext 'seven-card run' do
      let(:card_one) { CardCore::Card.new(:spade, :ace) }
      let(:card_two) { CardCore::Card.new(:heart, :two) }
      let(:card_three) { CardCore::Card.new(:diamond, :three) }
      let(:card_four) { CardCore::Card.new(:club, :four) }
      let(:card_five) {CardCore::Card.new(:heart, :five) }
      let(:card_six) { CardCore::Card.new(:diamond, :six) }
      let(:card_seven) { CardCore::Card.new(:spade, :seven) }


      context 'in any order' do
        let(:orderings) { [card_one, card_two, card_three, card_four, card_five, card_six, card_seven].permutation }

        it 'returns 7' do
          expect(orderings.map { |list| CardCore::Cribbage.peg_run_score(list) }).to all(eql 7)
        end
      end
    end
  end

  describe 'peg_score' do
    let(:card_two) { CardCore::Card.new(:spade, :jack) }
    let(:card_three) { CardCore::Card.new(:spade, :ace) }
    let(:card_four) { CardCore::Card.new(:heart, :queen) }

    context 'no score' do
      let(:card_two) { CardCore::Card.new(:heart, :two) }

      it 'returns 0' do
        expect(CardCore::Cribbage.peg_score([card_one, card_two])).to eql 0
      end
    end

    context 'count and match' do
      it 'returns 4' do
        expect(CardCore::Cribbage.peg_score([card_three, card_four, card_one, card_two])).to eql 4
      end
    end

    context 'count and run' do
      let(:card_two) { CardCore::Card.new(:diamond, :king) }

      it 'returns 5' do
        expect(CardCore::Cribbage.peg_score([card_three, card_one, card_two, card_four])).to eql 5
      end
    end
  end

  describe 'show_count_score' do
    let(:card_two) { CardCore::Card.new(:spade, :eight) }
    let(:card_three) { CardCore::Card.new(:heart, :two) }
    let(:card_four) { CardCore::Card.new(:diamond, :four) }
    let(:card_five) { CardCore::Card.new(:spade, :two) }

    context 'no score' do
      it 'returns 0' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 0
      end
    end

    context 'one set of 15' do
      let(:card_two) { CardCore::Card.new(:space, :ace) }
      let(:card_three) { CardCore::Card.new(:diamond, :ace) }
      let(:card_four) { CardCore::Card.new(:diamond, :five) }
      let(:card_five) { CardCore::Card.new(:club, :ace) }

      it 'returns 2' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 2
      end
    end

    context 'two sets of 15 using 1 repeating card' do
      let(:card_two) { CardCore::Card.new(:heart, :queen) }
      let(:card_three) { CardCore::Card.new(:heart, :five) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 4
      end
    end

    context 'one set of 31' do
      let(:card_two) { CardCore::Card.new(:heart, :queen) }
      let(:card_three) { CardCore::Card.new(:spade, :king) }
      let(:card_four) { CardCore::Card.new(:spade, :ace) }

      it 'returns 2' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 2
      end
    end

    context 'two sets of 31 using 1 repeating card' do
      let(:card_five) { CardCore::Card.new(:diamond, :ace) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 4
      end
    end

    context 'one set of 15 and one set of 31' do
      let(:card_two) { CardCore::Card.new(:spade, :two) }
      let(:card_three) { CardCore::Card.new(:heart, :six) }
      let(:card_four) { CardCore::Card.new(:club, :queen) }
      let(:card_five) { CardCore::Card.new(:club, :nine) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_count_score([card_one, card_two, card_three, card_four, card_five])).to eql 4
      end
    end
  end

  describe 'show_match_score' do
    let(:card_two) { CardCore::Card.new(:diamond, :jack) }
    let(:card_three) { CardCore::Card.new(:heart, :four) }
    let(:card_four) { CardCore::Card.new(:diamond, :seven) }
    let(:card_five) { CardCore::Card.new(:spade, :eight) }

    context 'no match' do
      let(:card_two) { CardCore::Card.new(:spade, :two) }

      it 'returns 0' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 0
      end
    end

    context '1 pair' do

      it 'returns 2' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 2
      end
    end

    context '2 pairs' do
      let(:card_four) { CardCore::Card.new(:club, :eight) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 4
      end
    end

    context 'set of three' do
      let(:card_three) { CardCore::Card.new(:club, :jack) }

      it 'returns 6' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 6
      end
    end

    context 'set of three and a pair' do
      let(:card_three) { CardCore::Card.new(:club, :jack) }
      let(:card_four) { CardCore::Card.new(:club, :eight) }

      it 'returns 8' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 8
      end
    end

    context 'set of four' do
      let(:card_three) { CardCore::Card.new(:club, :jack) }
      let(:card_four) { CardCore::Card.new(:spade, :jack) }

      it 'returns 12' do
        expect(CardCore::Cribbage.show_match_score([card_one, card_two, card_three, card_four, card_five])).to eql 12
      end
    end
  end

  describe 'show_run_score' do
    let(:card_two) { CardCore::Card.new(:spade, :ten) }
    let(:card_three) { CardCore::Card.new(:diamond, :nine) }
    let(:card_four) { CardCore::Card.new(:club, :eight) }
    let(:card_five) { CardCore::Card.new(:heart, :seven) }

    context 'no run' do
      let(:card_three) { CardCore::Card.new(:spade, :ace) }

      it 'returns 0' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 0
      end
    end

    context 'run of three' do
      let(:card_four) { CardCore::Card.new(:spade, :five) }

      it 'returns 3' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 3
      end
    end

    context 'two runs of three' do
      let(:card_four) { CardCore::Card.new(:diamond, :ten) }

      it 'returns 6' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 6
      end
    end

    context 'run of four' do
      let(:card_five) { CardCore::Card.new(:spade, :five) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 4
      end
    end

    context 'two runs of four' do
      let(:card_five) { CardCore::Card.new(:spade, :nine) }


      it 'returns 8' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 8
      end
    end

    context 'one run of five' do
      it 'returns 5' do
        expect(CardCore::Cribbage.show_run_score([card_one, card_two, card_three, card_four, card_five])).to eql 5
      end
    end
  end

  describe 'show_flush_score' do
    let(:card_two) { CardCore::Card.new(:heart, :ace) }
    let(:card_three) { CardCore::Card.new(:heart, :two) }
    let(:card_four) { CardCore::Card.new(:heart, :five) }
    let(:card_five) { CardCore::Card.new(:heart, :seven) }

    context 'no flush' do
      let(:card_three) { CardCore::Card.new(:spade, :two) }
      let(:card_five) { CardCore::Card.new(:diamond, :eight) }

      it 'returns 0' do
        expect(CardCore::Cribbage.show_flush_score([card_one, card_two, card_three, card_four], card_five)).to eql 0
      end
    end

    context '4-card flush' do
      let(:card_five) { CardCore::Card.new(:spade, :seven) }

      it 'returns 4' do
        expect(CardCore::Cribbage.show_flush_score([card_one, card_two, card_three, card_four], card_five)).to eql 4
      end
    end

    context '5-card flush' do
      it 'returns 5' do
        expect(CardCore::Cribbage.show_flush_score([card_one, card_two, card_three, card_four], card_five)).to eql 5
      end
    end
  end

  describe 'show_nob_score' do
    let(:starter_card) { CardCore::Card.new(:heart, :four) }
    let(:card_two) { CardCore::Card.new(:spade, :five) }
    let(:card_three) { CardCore::Card.new(:diamond, :eight) }
    let(:card_four) { CardCore::Card.new(:diamond, :queen) }

    context 'no nob' do
      let(:card_one) { CardCore::Card.new(:club, :seven) }

      it 'returns 0' do
        expect(CardCore::Cribbage.show_nob_score([card_one, card_two, card_three, card_four], starter_card)).to eql 0
      end
    end

    context 'nob' do
      it 'returns 1' do
        expect(CardCore::Cribbage.show_nob_score([card_one, card_two, card_three, card_four], starter_card)).to eql 1
      end
    end
  end
end
