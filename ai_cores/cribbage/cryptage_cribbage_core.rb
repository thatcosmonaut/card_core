module CardCore
  class CryptageCribbageCore < BaseCribbageCore
    def self.name
      "Cryptage"
    end

    def self.discard_choices(legal_moves)
      set_scores = {}
      legal_moves.combination(4).each do |set|
        set_scores[set] = CardCore::Cribbage.score_show_without_starter(set)
      end
      set_choice = set_scores.sort_by { |set, score| score }.last.first
      legal_moves - set_choice
    end

    def self.peg_choice(legal_moves, peg_card_list)
      peg_count = CardCore::Cribbage.peg_count(peg_card_list)

      move_scores = {}
      legal_moves.each do |card|
        move_scores[card] = CardCore::Cribbage.peg_score(peg_card_list + [card])
      end

      if move_scores.all? { |card, score| score == 0 }
        if peg_count < 15
          difference = 15 - peg_count
          legal_moves.each do |card|
            return card if Cribbage.card_score_value(card) > difference
          end
        end
        return legal_moves.sort_by { |card| Cribbage.card_score_value(card) }.last
      else
        return move_scores.sort_by { |card, score| score }.last.first
      end
    end
  end
end
